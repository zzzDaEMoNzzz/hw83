const express = require('express');
const upload = require('../upload');
const Album = require('../models/Album');

const router = express.Router();

router.get('/', (req, res) => {
  const artist = req.query.artist;

  Album.find(artist ? {artist} : null)
    .then(albums => res.send(albums))
    .catch(() => res.sendStatus(500));
});

router.get('/:id', (req, res) => {
  Album.findById(req.params.id).populate('artist')
    .then(album => {
      if (album) res.send(album);
      else res.sendStatus(404);
    })
    .catch(() => res.sendStatus(500));
});

router.post('/', upload.single('image'), (req, res) => {
  const albumData = req.body;

  if (req.file) {
    albumData.image = req.file.filename;
  }

  const album = new Album(albumData);

  album.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));
});


module.exports = router;
