const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const User = require('../models/User');

const router = express.Router();

router.post('/', async (req, res) => {
  const token = req.get('Token');

  const user = await User.findOne({token});

  if (!user) {
    return res.sendStatus(401);
  }

  const trackHistoryData = req.body;
  trackHistoryData.user = user._id;
  trackHistoryData.datetime = new Date().toISOString();

  const trackHistory = new TrackHistory(trackHistoryData);

  try {
    await trackHistory.save();
    return res.send(trackHistory);
  } catch (error) {
    return res.status(400).send(error);
  }
});

module.exports = router;