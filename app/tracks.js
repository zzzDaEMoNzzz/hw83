const express = require('express');
const Track = require('../models/Track');
const Album = require('../models/Album');

const router = express.Router();

router.get('/', (req, res) => {
  const album = req.query.album;

  Track.find(album ? {album} : null)
    .then(tracks => res.send(tracks))
    .catch(() => res.sendStatus(500));
});

router.get('/:artist', (req, res) => {
  Album.find({artist: req.params.artist}).then(albums => {
    const album_ids = albums.map(album => album._id);

    Track.find({album: {$in: album_ids}})
      .then(tracks => res.send(tracks))
      .catch(() => res.sendStatus(500));
  }).catch(() => res.sendStatus(500));
});

module.exports = router;