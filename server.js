const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const albums = require('./app/albums');
const artists = require('./app/artists');
const tracks = require('./app/tracks');
const users = require('./app/users');
const track_history = require('./app/track_history');
const config = require("./config");

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
  app.use('/albums', albums);
  app.use('/artists', artists);
  app.use('/tracks', tracks);
  app.use('/users', users);
  app.use('/track_history', track_history);

  app.listen(port, () => {
    console.log(`Server started on ${port} port`);
  });
});
